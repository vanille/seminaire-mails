TWICE = true

DOC = mail

BUILDER = texmake.py

all: $(DOC:%=%.pdf)

doc_%.tex.mk: cfg_%.tmk $(BUILDER)
	./$(BUILDER) name=$* fail=WARNING

common.tex.mk: $(BUILDER)
	./$(BUILDER) common

texwatch: $(BUILDER)
	./$(BUILDER) watcher
	

-include $(DOC:%=doc_%.tex.mk)
-include common.tex.mk

ARCHIVE=archive

clean:
	rm -rf build
	rm -f *.tex.mk
	rm -f $(DOC:%=%.pdf)
	rm -rf $(ARCHIVE) $(ARCHIVE).tar.gz

tar:
	make clean
	mkdir $(ARCHIVE)
	cp -r src $(ARCHIVE)
	cp Makefile texwatch $(ARCHIVE)
	cp cfg_mail.tmk texmake.py $(ARCHIVE)
	tar czf $(ARCHIVE).tar.gz $(ARCHIVE)

force:
	make clean
	make
