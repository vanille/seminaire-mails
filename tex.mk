# LaTeX Makefile
# Neven Villani <neven.villani@gmail.com>
# Jul. 2021
# 
# Reusable make configuration for LaTeX compilation
# Features:
# 	* separate build directory
# 	* absolute paths relative to git root
# 	* real-time compilation

GITROOT = $(shell git rev-parse --show-toplevel)

TEXFLAGS = --interaction=nonstopmode --halt-on-error
TEXC = pdflatex $(TEXFLAGS)
BIBLIO = $(GITROOT)/docs/refs.bib
MAKES = Makefile $(GITROOT)/redac/tex.mk

main.pdf: \
	$(FIGPDF:%=build/%.pdf) \
	$(CHAPTERS:%=build/%.tex) \
	$(HEADER:%=build/%.tex) \
	build/main.tex \
	$(BIBLIO) \
	$(MAKES)
	#
	make compile-main
	@if [ -n "$(HASBIB)" ] && [ -z "$(QUICK)" ]; then \
		cp $(BIBLIO) build ; \
		( cd build && bibtex main ) ; \
		make compile-main ; \
	fi
	@if [ -n "$(TWICE)$(HASBIB)" ] && [ -z "$(QUICK)" ]; then \
		make compile-main ; \
	fi
	cp build/main.pdf .

compile-main: build/main.tex
	make FILE=main.tex compile

build/%.pdf: build/%.tex
	make FILE=$(<:build/%=%) compile

compile:
	cd build && $(TEXC) $(FILE) | \
		grep -Ev 'texmf-dist|\.code\.tex|\.dict|^[^(]*\)' | \
		sed '/^[[:space:]]*$$/d'

build/%.tex: src/%.tex |build
	cp $< build

build/%.tex: figures/%.tex |build
	cp $< build

build/%.tex: $(GITROOT)/redac/incl/%.tex |build
	cp $< build

build:
	mkdir -p build

TARGET = main.pdf
loop:
	$(GITROOT)/redac/texwatch $(TARGET)

clean:
	rm -rf build

reset: clean
	rm -f main.pdf

force:
	make reset
	make main.pdf
	

.SUFFIXES:

.PHONY: compile-main clean reset force compile
